﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : ButtonElement {

    public TimeController timeController;
    public ScoreController scoreController;
    public HealthController healthController;

    private void Update()
    {
     
    }

    public void ButtonPress()
    {
       

        RaycastHit hitInfo = new RaycastHit();
        bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo);
        if (hit)
        {
            
            if (hitInfo.transform.gameObject.tag == "RedButton" && app.model.gameOver == false)
            {
                app.controller.healthController.TakeDamge();
            }
            if(hitInfo.transform.gameObject.tag == "GreenButton" && app.model.gameOver == false)
            {
                app.controller.scoreController.AddPoint();
            }

        }
    }

    public void ButtonShuffle()
    {
        for (int i = 0; i < app.model.buttons.Length; i++)
        {

            int k = Random.Range(0, app.model.buttons.Length);
            Vector2 tmp = app.model.buttons[i].transform.position;
            app.model.buttons[i].transform.position = app.model.buttons[k].transform.position;
            app.model.buttons[k].transform.position = tmp;

        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
