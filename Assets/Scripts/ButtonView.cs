﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonView : ButtonElement
{
    public TimeView timeView;
    public ScoreView scoreView;
    public HealthView healthView;

    

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && app.model.gameOver == false)
        {
            app.controller.ButtonPress();
            app.controller.ButtonShuffle();
        }

        if(app.model.timeM.time == 0 || app.model.healthModel.currentHealth == 0)
        {
            app.model.gameOver = true;
            app.model.timeM.timeText.enabled = false;
            app.model.scoreModel.scoreText.enabled = false;
            app.controller.scoreController.ShowPopup();
            app.model.buttonsParent.SetActive(false);
            app.controller.scoreController.SavePoints();


            
        }
    }
    
}
