﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : ButtonElement
{

    
    public void SetHealth()
    {
       
        app.model.healthModel.currentHealth = app.model.healthModel.health;

      
        
    }

    public void TakeDamge()
    {
        Destroy(app.model.healthModel.heartsList[app.model.healthModel.currentHealth -1]);
        app.model.healthModel.currentHealth -= 1;
        app.model.healthModel.heartsList.RemoveRange(app.model.healthModel.currentHealth, 1);
       
    }
}
