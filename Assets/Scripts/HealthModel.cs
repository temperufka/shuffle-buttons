﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthModel : ButtonElement
{
    public int health = 0;
    public int currentHealth = 0;
    
    public List<GameObject> heartsList;

}
