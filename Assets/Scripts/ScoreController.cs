﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : ButtonElement
{
    
    public void AddPoint()
    {
       
        app.model.scoreModel.score += 1;
    }

    public void ShowPopup()
    {
        
        app.model.scoreModel.popUp.GetComponent<CanvasGroup>().alpha = 1;
        app.model.scoreModel.popUp.GetComponent<CanvasGroup>().interactable = true;
        app.model.scoreModel.popUp.GetComponent<CanvasGroup>().blocksRaycasts = true;
        app.model.scoreModel.popUp.GetComponent<CanvasGroup>().ignoreParentGroups = true;
    }

    public void SavePoints()
    {
        PlayerPrefs.SetInt(app.model.scoreModel.highScoreKey, app.model.scoreModel.score);
        PlayerPrefs.Save();
    }

}
