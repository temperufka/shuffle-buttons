﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreModel : ButtonElement {

    public int score = 0;
    public Text scoreText;
    public Text popUpText;
    public string highScoreKey = "HighScore";
    public GameObject popUp;
}
