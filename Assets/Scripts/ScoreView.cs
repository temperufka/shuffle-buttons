﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreView : ButtonElement {
    

    private void Update()
    {
        app.model.scoreModel.scoreText.text = "Score: " + app.model.scoreModel.score;
        app.model.scoreModel.popUpText.text = "Score: " + app.model.scoreModel.score;

    }
}
