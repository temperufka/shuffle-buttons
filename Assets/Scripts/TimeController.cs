﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeController : ButtonElement
{
    
    public void Timer()
    { 
        if (app.model.timeM.reduceTime == true && app.model.timeM.time > 0)
        {
            
            app.model.timeM.time -= Time.deltaTime;
            

        }

        else
        {
            app.model.timeM.reduceTime = false;
            app.model.timeM.time = 0;
        }
       
      
	}
}
