﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeMenuView : ButtonElement {

	public Text Timtext;
    public GameObject StartGameText;
    public GameObject GameView;
    public GameObject MenuCanvas;
    int StartGameValue = 3;
	void Start () 
	{
        
        Timtext.text = app.model.timeM.time.ToString("f1");
        
        
	}


	public void ReduceTime()
	{
		if(app.model.timeM.time > 10)
		{
		app.model.timeM.time -= 5;
		Timtext.text = app.model.timeM.time.ToString("f1");
		}
	}

	public void AddTime()
	{
		app.model.timeM.time += 5;
		Timtext.text = app.model.timeM.time.ToString("f1");
	}

    private IEnumerator StartGame()
    {
        float timer = 3f;
        
        while(timer <= 3)
        {
            float deltaT = Time.deltaTime*23;
            timer -= deltaT;
            float percent = timer;
            //percent /= 2;
            StartGameText.SetActive(true);
            StartGameText.GetComponent<Text>().text = percent.ToString("f0");
            if(timer <=0)
            {
                StartGameText.SetActive(false);
                GameView.SetActive(true);
                
                StopAllCoroutines();
            }
            yield return new WaitForSeconds(deltaT);
        }
        
    }

    public void Play()
    {
        MenuCanvas.SetActive(false);
        StartCoroutine(StartGame());
        
    }
}
