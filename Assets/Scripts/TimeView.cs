﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeView : ButtonElement
{

    private void Update()
    {
        if (app.model.gameOver == false)
        {
            app.controller.timeController.Timer();
            app.model.timeM.timeText.text = "Time : " + app.model.timeM.time.ToString("f1");
        }
    }

}
